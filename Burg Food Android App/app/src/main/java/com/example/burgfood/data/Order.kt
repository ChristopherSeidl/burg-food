package com.example.burgfood.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

/**
 *
 * Order - Stores what the order options area and which options have been selected. Along with this
 *        it stores the items that have been selected.
 *
 */


@Parcelize
data class Order(val data : @RawValue Array<FoodItem>, val orderType : Int) : Parcelable {
    //order type 0 = packed, 1= hot, 2 = dinner
    var selectedType1: @RawValue FoodItem? = null
    var selectedType0: @RawValue MutableMap<String, FoodItem> =  mutableMapOf()
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Order

        if (!data.contentEquals(other.data)) return false
        if (orderType != other.orderType) return false
        if (selectedType1 != other.selectedType1) return false
        if (selectedType0 != other.selectedType0) return false

        return true
    }

    override fun hashCode(): Int {
        var result = data.contentHashCode()
        result = 31 * result + orderType
        result = 31 * result + (selectedType1?.hashCode() ?: 0)
        result = 31 * result + selectedType0.hashCode()
        return result
    }

}