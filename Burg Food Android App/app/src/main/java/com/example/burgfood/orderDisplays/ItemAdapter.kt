package com.example.burgfood.orderDisplays

//import com.example.burgfood.orderDisplays.packedLunch
import android.content.Context
import android.icu.util.Calendar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.AnimationSet
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.burgfood.R
import com.example.burgfood.data.CutOffTimes
import com.example.burgfood.data.Order
import java.text.SimpleDateFormat
import java.util.*

/**
 *
 * ItemAdapter - Handles the display of food items in the recycler views.
 *               scale - how many px 1 dp is.
 *               confirmButton - reference to the confirm button
 *               order - data of what to be displayed. Also acts as the data structure that stores
 *               what was selected.
 *
 */


class ItemAdapter(private var order: Order, private val context: Context, private val confirmButton: Button, private var scale: Int, private var cutOff: Date) : RecyclerView.Adapter<ItemAdapter.ItemViewHolder>() {

    // The currently selected item - Used to ensure that only one type 1 item is selected at a time.
    private var lastSelected: ItemViewHolder? = null

    private var currentTime: Date = Calendar.getInstance().time

    // On creation of a row
    override fun onBindViewHolder(itemViewHolder: ItemViewHolder, index: Int) {
        // Get relevant data
        val foodItem = order.data[index]

        itemViewHolder.locationInFoodArray = index
        itemViewHolder.frame.tag = foodItem.index
        itemViewHolder.backgroundContainer.setImageResource(foodItem.type.resource.resource)
        itemViewHolder.backgroundContainer.setColorFilter(foodItem.type.resource.overlay, android.graphics.PorterDuff.Mode.MULTIPLY)
        itemViewHolder.textContainer.text = foodItem.type.string
        itemViewHolder.frame.setOnClickListener {
            selectItem(itemViewHolder)
        }
        itemViewHolder.selectorContainer.tag = "selected"

        // Check if the item is already selected

        if (foodItem.selected) {

            // Draw tick and log it as the selected type 1 item.
            itemViewHolder.selectorContainer.setImageResource(R.drawable.tick)
            if (foodItem.type.type == 1) {
                lastSelected = itemViewHolder
            }

        }
        // Log.d("myTag", "Create")
    }

    private fun selectItem(itemViewHolder: ItemViewHolder) {

        // image view that displays the tick/selector
        val selected = itemViewHolder.selectorContainer

        val currentDataReferenceInMenu = order.data[itemViewHolder.locationInFoodArray]

        // animationDuration for both fade in and out
        val animationDuration : Long = 300

        // Is item already selected
        if (currentDataReferenceInMenu.selected){

            // Fade Out Animation
            val fadeOut = AlphaAnimation(1f, 0f)
            fadeOut.interpolator = AccelerateInterpolator()
            fadeOut.duration = animationDuration
            val animation = AnimationSet(false)
            animation.setAnimationListener(object: Animation.AnimationListener {
                override fun onAnimationRepeat(animation: Animation?) {}
                override fun onAnimationEnd(animation: Animation?) {
                    selected.setImageResource(android.R.color.transparent)
                }
                override fun onAnimationStart(animation: Animation?) {}
            })
            animation.addAnimation(fadeOut)
            selected.startAnimation(animation)

            // Update data structure
            currentDataReferenceInMenu.selected = false

            // Type 1
            if (currentDataReferenceInMenu.type.type == 1) {
                lastSelected = null
                order.selectedType1 = null
            } else {

               // Type 0
                order.selectedType0.remove(currentDataReferenceInMenu.type.string)
            }

            //Check if confirm orderButtonShould be closed
            if (order.selectedType1 == null && order.selectedType0.isEmpty()) {

                confirmButton.layoutParams.height = 0
            }

        } else {
            // Item is not already selected
            // Fade In Animation

            // Set the transparency then set the image





            // Fade in animation
            val fadeIn = AlphaAnimation(0f, 1f)
            fadeIn.interpolator = AccelerateInterpolator()
            fadeIn.duration = animationDuration
            val animation = AnimationSet(false)

            animation.setAnimationListener(object: Animation.AnimationListener {
                override fun onAnimationRepeat(animation: Animation?) {}
                override fun onAnimationEnd(animation: Animation?) {


//                    selected.alpha = 1f

                }
                override fun onAnimationStart(animation: Animation?) {
//                    selected.alpha = 0f
                    selected.setImageResource(R.drawable.tick)
                }
            })

            // Do the fade in animation
            animation.addAnimation(fadeIn)
            selected.startAnimation(animation)


            // * Bring up the Button *
            // Check the cut off time and update the button
            val format = SimpleDateFormat("h a")

            // Determine cutoff time & set appropriate text
            if (currentTime.after(cutOff)) {
                when (order.orderType) {
                    0 -> {

                        val completionDate = Date(currentTime.year, currentTime.month, currentTime.date, CutOffTimes.packedLunchCollectionTime.hours, CutOffTimes.packedLunchCollectionTime.minutes)
                        confirmButton.text = "Confirm Order for " + format.format(completionDate) +  " Tomorrow"
                    }
                    1 -> {

                        val completionDate = Date(currentTime.year, currentTime.month, currentTime.date, CutOffTimes.hotLunchCollectionTime.hours, CutOffTimes.hotLunchCollectionTime.minutes)
                        confirmButton.text = "Confirm Order for " + format.format(completionDate) +  " Tomorrow"
                    }
                    else -> {

                        val completionDate = Date(currentTime.year, currentTime.month, currentTime.date, CutOffTimes.dinnerCollectionTime.hours, CutOffTimes.dinnerCollectionTime.minutes)
                        confirmButton.text = "Confirm Order for " + format.format(completionDate) +  " Tomorrow"
                    }
                }

            } else {
                when (order.orderType) {
                    0 -> {

                        val completionDate = Date(currentTime.year, currentTime.month, currentTime.date, CutOffTimes.packedLunchCollectionTime.hours, CutOffTimes.packedLunchCollectionTime.minutes)
                        confirmButton.text = "Confirm Order for " + format.format(completionDate) +  " Today"
                    }
                    1 -> {

                        val completionDate = Date(currentTime.year, currentTime.month, currentTime.date, CutOffTimes.hotLunchCollectionTime.hours, CutOffTimes.hotLunchCollectionTime.minutes)
                        confirmButton.text = "Confirm Order for " + format.format(completionDate) +  " Today"
                    }
                    else -> {

                        val completionDate = Date(currentTime.year, currentTime.month, currentTime.date, CutOffTimes.dinnerCollectionTime.hours, CutOffTimes.dinnerCollectionTime.minutes)
                        confirmButton.text = "Confirm Order for " + format.format(completionDate) +  " Today"
                    }
                }
            }

            // Show the button
            confirmButton.layoutParams.height = 75 * scale

            // Backend
            currentDataReferenceInMenu.selected = true

            // Handle type 1 - Deselect the any other type 1 items
            if (currentDataReferenceInMenu.type.type == 1) {

//                // Fade Out Animation
//                val fadeOut = AlphaAnimation(1f, 0f)
//                fadeOut.interpolator = AccelerateInterpolator()
//                fadeOut.duration = animationDuration
//                val animation = AnimationSet(false)
//                animation.setAnimationListener(object: Animation.AnimationListener {
//                    override fun onAnimationRepeat(animation: Animation?) {}
//                    override fun onAnimationEnd(animation: Animation?) {
//                        selected.setImageResource(android.R.color.transparent)
//                    }
//                    override fun onAnimationStart(animation: Animation?) {}
//                })
//                animation.addAnimation(fadeOut)
//                lastSelected?.selectorContainer?.startAnimation(animation)
                lastSelected?.selectorContainer?.setImageResource(android.R.color.transparent)


                // Update the order/backend
                if (lastSelected != null) {
                    order.data[lastSelected?.locationInFoodArray!!].selected = false
                }
                order.selectedType1 = order.data[itemViewHolder.locationInFoodArray]
                lastSelected = itemViewHolder

            }else {

                // Handle type 0 - add the item to the map
                order.selectedType0[currentDataReferenceInMenu.type.string] =
                    currentDataReferenceInMenu
            }
        }
        // Log.d("myTag", "Click")
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(LayoutInflater.from(context).inflate(R.layout.food_item_layout, parent, false))
    }

    override fun getItemCount(): Int {
        return order.data.size
    }


    inner class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val frame: ConstraintLayout = view.findViewById(R.id.item)
        val backgroundContainer : ImageView = view.findViewById(R.id.background)
        val selectorContainer : ImageView = view.findViewById(R.id.selector)
        val textContainer : TextView = view.findViewById(R.id.itemText)
        var locationInFoodArray : Int = 0
    }

}
