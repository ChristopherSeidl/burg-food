package com.example.burgfood

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

/**
 *
 *  AccessCodeActivity - Where the user enters the access code.
 *
 */

class AccessCodeActivity : AppCompatActivity() {

    private var correctAccessCode: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_access_code)

        // Get access code from the backend
        val sharedPref = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE) ?: return
        val defaultValue = ""
        val aCode = sharedPref.getString("accessCode", defaultValue)
        correctAccessCode = sharedPref.getString("correctAccessCode", null)
        Log.d("MyTag", "Correct Code is:  $correctAccessCode")

        if (aCode == correctAccessCode) {
            val intent = Intent(this, LogInActivity::class.java).apply {}
            startActivity(intent)
        }
    }


    fun submitAnswer(view: View) {
        // Access code entered by the user
        val aCode = findViewById<TextInputEditText>(R.id.logInTextFieldInner).text.toString()

        // Authentication
        // Get the correct code
        val sharedPref = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE) ?: return
        correctAccessCode = sharedPref.getString("correctAccessCode", null)

        // Log.d("MyTag", "Correct Code is:  $correctAccessCode")

        // If we could not connect to the server we cannot authenticate
        if (correctAccessCode == null) {

            findViewById<TextInputLayout>(R.id.logInTextField).error = "Unable to connect to the authentication Server"

        } else {

            // Check that the code is correct
            if (aCode == correctAccessCode.toString()) {

                // Reset the text view
                findViewById<TextInputLayout>(R.id.logInTextField).error = null

                // Put Access Code in shared preference to save it.
                with(sharedPref.edit()) {
                    putString("accessCode", aCode)
                    commit()
                }

                // Login Activity
                val intent = Intent(this, LogInActivity::class.java).apply {}
                startActivity(intent)

            } else {

                // Display to the user that they entered the wrong code.
                findViewById<TextInputLayout>(R.id.logInTextField).error = "Invalid Access Code"

            }
        }
    }

    // Disable back button
    override fun onBackPressed() {}
}
