package com.example.burgfood.orderDisplays

import android.animation.LayoutTransition
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.example.burgfood.LogInActivity
import com.example.burgfood.R
import kotlinx.android.synthetic.main.activity_select_meal_view.*

/**
 *
 * SelectMealViewActivity - View that holds the tabs for each type of order.
 *
 */

class SelectMealViewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_meal_view)

        // Allow animated layout changes
        findViewById<CoordinatorLayout>(R.id.selectMealViewCordinator).layoutTransition.enableTransitionType(
            LayoutTransition.CHANGING)

        // NO CLUE CAME WITH PRESET
        val fragmentAdapter =
            SectionsPagerAdapter(supportFragmentManager)
        view_pager.adapter = fragmentAdapter
        tabs.setupWithViewPager(view_pager)

        // Getting the current username. And setting it to the username text field
        val sharedPref = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE) ?: return
        val defaultValue = "Burg Food"
        val username = sharedPref.getString("Username", defaultValue)
        if (username == "") {
            findViewById<TextView>(R.id.usernameLabel).text = "Burg Food"
        }else {
            findViewById<TextView>(R.id.usernameLabel).text = username.toString()
        }

    }

    // Always go back to login screen
    override fun onBackPressed() {
        val intent = Intent(this, LogInActivity::class.java).apply {}
        startActivity(intent)
    }

    public fun back(view: View) {
        onBackPressed()
    }

}