package com.example.burgfood.data

import java.util.*

data class OrderHistoryItem (
    var orderType: String,
    var orderInstanceId: String,
    val foodItems: ArrayList<FoodTypes>,
    val orderTime: Date) : Comparable <OrderHistoryItem> {

    override fun compareTo(other: OrderHistoryItem): Int {
        return if (this.orderTime.before(other.orderTime)) {
            -1
        } else if (this.orderTime.after(other.orderTime)){
            1
        } else {


            var otherOrderTypeInt = when (other.orderType) {
                "Packed Lunch" -> 0
                "Hot Lunch" -> 1
                else -> 2
            }
            var selfOrderTypeInt = when (this.orderType) {
                "Packed Lunch" -> 0
                "Hot Lunch" -> 1
                else -> 2
            }

            return if (selfOrderTypeInt < otherOrderTypeInt) {
                -1
            } else if (selfOrderTypeInt == otherOrderTypeInt) {
                0
            } else {
                1
            }
        }
    }

    fun getOrderClassName() : String {
        return when (orderType) {
            "Packed Lunch" -> "PackedLunch"
            "Hot Lunch" -> "HotLunch"
            else -> "Dinner"
        }
    }

    fun getOrderString() : String {
        var orderString = ""
        foodItems.forEach {
            orderString = orderString + ", " + it.string.replace("\n", "")
        }
        orderString = orderString.drop(2)

        return orderString

    }




}