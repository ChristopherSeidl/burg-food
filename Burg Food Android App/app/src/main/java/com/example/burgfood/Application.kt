package com.example.burgfood

import android.app.Application
import android.content.Context
import android.util.Log
import com.parse.Parse
import com.parse.ParseObject
import com.parse.ParseQuery

/**
 *
 * Application - Overarching activity that is always running parallel to the flow of the app.
 *
*/

class Application : Application() {

    // Initializes Parse SDK as soon as the application is created
    override fun onCreate() {
        super.onCreate()
        Parse.initialize(
            Parse.Configuration.Builder(this)
                .applicationId("72QdeuUQI9G9ngQUG1tbM4fi4rIG5xYMoFkucnn2")
                .clientKey("8viP7V513sGXo1F02OBPZN7UF5IzyaqnTquIjeqn")
                .server("https://parseapi.back4app.com")
                .build()
        )

        // Log.d("MyTag", "Application Created")
        getPinFromOnline()

    }

    // Gets the pin from online.
    private fun getPinFromOnline() {
        val query = ParseQuery.getQuery<ParseObject>("BurgPIN")

        // The query will search for a ParseObject, given its objectId.
        // When the query finishes running, it will invoke the GetCallback
        // with either the object, or the exception thrown
        query.getFirstInBackground { result, e ->
            // Log.d("MyTag", "returned from parse backend")

            if (e == null) { // No Error

                var pin = result.getString("PIN")
                Log.d("MyTag", "Pin is : $pin")

                // Put Username in shared preference to save it.
                val sharedPref = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE) ?: return@getFirstInBackground
                with(sharedPref.edit()) {
                    putString("correctAccessCode", pin)
                    commit()
                }


            } else { // something went wrong
                Log.d("MyTag", "Error : " + e.code + " - " + e.message)
            }
        }
    }



}
